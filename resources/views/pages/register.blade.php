<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form Register</title>
</head>
<body>
    <b><h1>Buat Account Baru!</h1></b>
        <b><h3>Sign Up Form</h3></b>

        <form action="/welcome" method="POST">
            @csrf
            <div>
                <p>First Name:</p>
                <input type="text" name="fname" id="fname"><br>
            </div>
            
            <div>
                <p>Last Name: </p>
                <input type="text" name="lname" id="lname"><br>
            </div>

            <div>
                <p>Gender</p>
                <input type="radio" name="gender" id="gender" value="Laki-laki"><label for="gender">Laki-laki</label><br>
                <input type="radio" name="gender" id="gender" value="Perempuan"><label for="gender">Perempuan</label>
            </div>

            <div>
                <p>Nationality</p>
                <select name="nationality" id="nationality">
                    <option value="Indonesia">Indonesia</option>
                    <option value="WNA">WNA</option>
                </select>
            </div>

            <div>
                <p>Language Spoken: </p>
                <input type="checkbox" name="language" id="language" value="Bahasa Indonesia">
                <label for="language"> Bahasa Indonesia</label><br>
                <input type="checkbox" name="language" id="language" value="English">
                <label for="language"> English</label><br>
                <input type="checkbox" name="language" id="language" value="Other">
                <label for="language"> Other</label><br>
            </div>

            <div>
                <p>Bio:</p>
                <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
                <input type="submit" name="submit" >

            </div>
        </form>
</body>
</html>